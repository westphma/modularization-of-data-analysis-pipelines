# !!!
# This script is adapted from a Jupyter Notebook created by Serkan Peldek.
# The original Notebook can be found on Kaggle: https://www.kaggle.com/code/serkanpeldek/object-oriented-titanics
# !!!

import numpy as np
import pandas as pd 

import matplotlib.pyplot as plt

import os
import warnings

from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import VotingClassifier

from yellowbrick.features import RadViz

class I():

    def __init__(self):
        print("Information object created")

    def _get_missing_values(self,data):
        missing_values = data.isnull().sum()
        missing_values.sort_values(ascending=False, inplace=True)
        
        return missing_values

    def info(self,data):
        feature_dtypes=data.dtypes
        self.missing_values=self._get_missing_values(data)

        print("=" * 50)

        print("{:16} {:16} {:25} {:16}".format("Feature Name".upper(),
                                            "Data Format".upper(),
                                            "# of Missing Values".upper(),
                                            "Samples".upper()))
        for feature_name, dtype, missing_value in zip(self.missing_values.index.values,
                                                      feature_dtypes[self.missing_values.index.values],
                                                      self.missing_values.values):
            print("{:18} {:19} {:19} ".format(feature_name, str(dtype), str(missing_value)), end="")
            for v in data[feature_name].values[:10]:
                print(v, end=",")
            print()

        print("="*50)


class P():

    def __init__(self):
        print("Preprocess object created")

    def fillna(self, data, fill_strategies):
        for column, strategy in fill_strategies.items():
            if strategy == 'None':
                data[column] = data[column].fillna('None')
            elif strategy == 'Zero':
                data[column] = data[column].fillna(0)
            elif strategy == 'Mode':
                data[column] = data[column].fillna(data[column].mode()[0])
            elif strategy == 'Mean':
                data[column] = data[column].fillna(data[column].mean())
            elif strategy == 'Median':
                data[column] = data[column].fillna(data[column].median())
            else:
                print("{}: There is no such thing as preprocess strategy".format(strategy))

        return data

    def drop(self, data, drop_strategies):
        for column, strategy in drop_strategies.items():
            data=data.drop(labels=[column], axis=strategy)

        return data

    def feature_engineering(self, data, engineering_strategies):
        if engineering_strategies==1:
            return self._feature_engineering1(data)
        else:
            return data

    def _feature_engineering1(self,data):

        data=self._base_feature_engineering(data)


        data['FareBin'] = pd.qcut(data['Fare'], 4)

        data['AgeBin'] = pd.cut(data['Age'].astype(int), 5)

        drop_strategy = {'Age': 1,  
                         'Name': 1,
                         'Fare': 1}
        data = self.drop(data, drop_strategy)

        return data

    def _base_feature_engineering(self,data):
        data['FamilySize'] = data['SibSp'] + data['Parch'] + 1

        data['IsAlone'] = 1
        data.loc[(data['FamilySize'] > 1), 'IsAlone'] = 0

        data['Title'] = data['Name'].str.split(", ", expand=True)[1].str.split('.', expand=True)[0]
        min_lengtht = 10
        title_names = (data['Title'].value_counts() < min_lengtht)
        data['Title'] = data['Title'].apply(lambda x: 'Misc' if title_names.loc[x] == True else x)

        return data

    def _label_encoder(self,data):
        labelEncoder=LabelEncoder()
        for column in data.columns.values:
            if 'int64'==data[column].dtype or 'float64'==data[column].dtype or 'int64'==data[column].dtype:
                continue
            labelEncoder.fit(data[column])
            data[column]=labelEncoder.transform(data[column])
        return data

    def _get_dummies(self, data, prefered_columns):

        if prefered_columns is None:
            columns=data.columns.values
            non_dummies=None
        else:
            non_dummies=[col for col in data.columns.values if col not in prefered_columns ]

            columns=prefered_columns


        dummies_dat=[pd.get_dummies(data[col],prefix=col) for col in columns]

        if non_dummies is not None:
            for non_dummy in non_dummies:
                dummies_dat.append(data[non_dummy])

        return pd.concat(dummies_dat, axis=1)

class S():

    def __init__(self):
        self.dats=None
        self._p=P()

    def strategy(self, datas, strategy_type):
        self.dats=datas
        self._strategy1()

        return self.dats

    def _base_strategy(self):
        drop_strategy = {'PassengerId': 1,  
                         'Cabin': 1,
                         'Ticket': 1}
        self.dats = self._p.drop(self.dats, drop_strategy)

        fill_strategy = {'Age': 'Median',
                         'Fare': 'Median',
                         'Embarked': 'Mode'}
        self.dats = self._p.fillna(self.dats, fill_strategy)

        self.dats = self._p.feature_engineering(self.dats, 1)


        self.dats = self._p._label_encoder(self.dats)

    def _strategy1(self):
        self._base_strategy()

        self.dats=self._p._get_dummies(self.dats,['Pclass', 'Sex', 'Parch', 'Embarked', 'Title', 'IsAlone'])

    def _strategy2(self):
        self._base_strategy()

        self.dats=self._p._get_dummies(self.dats,None)



class G():
    def __init__(self):
        print("GridSearchHelper Created")

        self.gridSearchCV=None
        self.clf_and_params=list()

        self._initialize_clf_and_params()

    def _initialize_clf_and_params(self):

        clf= KNeighborsClassifier()
        params={'n_neighbors':[5,7,9,11,13,15],
          'leaf_size':[1,2,3,5],
          'weights':['uniform', 'distance']
          }
        self.clf_and_params.append((clf, params))

        clf=LogisticRegression()
        params={'penalty':['l1', 'l2'],
                'C':np.logspace(0, 4, 10)
                }
        self.clf_and_params.append((clf, params))

        clf = SVC()
        params = [ {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
                   {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']}]
        self.clf_and_params.append((clf, params))

        clf=DecisionTreeClassifier()
        params={'max_features': ['auto', 'sqrt', 'log2'],
          'min_samples_split': [2,3,4,5,6,7,8,9,10,11,12,13,14,15],
          'min_samples_leaf':[1],
          'random_state':[123]}

        self.clf_and_params.append((clf,params))

        clf = RandomForestClassifier()
        params = {'n_estimators': [4, 6, 9],
              'max_features': ['log2', 'sqrt','auto'],
              'criterion': ['entropy', 'gini'],
              'max_depth': [2, 3, 5, 10],
              'min_samples_split': [2, 3, 5],
              'min_samples_leaf': [1,5,8]
             }

        self.clf_and_params.append((clf, params))

    def fit_predict_save(self, X_trains, X_tests, y_trains, submission_ids, strategy_types):
        self.X_train=X_trains
        self.X_test=X_tests
        self.y_train=y_trains
        self.submission_id=submission_ids
        self.strategy_type=strategy_types

        clf_and_params = self.get_clf_and_params()
        models=[]
        self.results={}
        for clf, params in clf_and_params:
            self.current_clf_name = clf.__class__.__name__
            grid_search_clf = GridSearchCV(clf, params, cv=5)
            grid_search_clf.fit(self.X_train, self.y_train)
            self.Y_pred = grid_search_clf.predict(self.X_test)
            clf_train_acc = round(grid_search_clf.score(self.X_train, self.y_train) * 100, 2)
            print(self.current_clf_name, " trained and used for prediction on test data...")
            self.results[self.current_clf_name]=clf_train_acc
            models.append(clf)

            self.save_result()
            print()
    
    def show_result(self):
        for clf_name, train_acc in self.results.items():
                  print("{} train accuracy is {:.3f}".format(clf_name, train_acc))
        
    def save_result(self):
        Submission = pd.DataFrame({'PassengerId': self.submission_id,
                                           'Survived': self.Y_pred})
        file_name="{}_{}.csv".format(self.strategy_type,self.current_clf_name.lower())
        Submission.to_csv(file_name, index=False)

        print("Submission saved file name: ",file_name)

    def get_clf_and_params(self):

        return self.clf_and_params

    def add(self,clfs, paramss):
        self.clf_and_params.append((clfs, paramss))


class V:
    
    def __init__(self):
        print("Visualizer object created!")
    
    def RandianViz(self, Xs, ys, number_of_features):
        if number_of_features is None:
            features=Xs.columns.values
        else:
            features=Xs.columns.values[:number_of_features]
        
        fig, ax=plt.subplots(1, figsize=(15,12))
        radViz=RadViz(classes=['survived', 'not survived'], features=features)
        
        radViz.fit(Xs, ys)
        radViz.transform(Xs)
        radViz.poof()

class O():

    def __init__(self, trains, tests):

        print("ObjectOrientedTitanic object created")
        self.testPassengerID=tests['PassengerId']
        self.number_of_train=trains.shape[0]

        self.y_train=trains['Survived']
        self.train=trains.drop('Survived', axis=1)
        self.test=tests

        self.all_data=self._get_all_data()

        self._i=I()
        self.pS = S()
        self.v=V()
        self.gSH = G()

    def _get_all_data(self):
        return pd.concat([self.train, self.test])

    def information(self):
        self._i.info(self.all_data)

    def preprocessing(self, strategy_types):
        self.strategy_type=strategy_types

        self.all_data = self.pS.strategy(self._get_all_data(), strategy_types)

    def visualize(self, visualizer_type, number_of_features=None):
        self._get_train_and_test()
        
        if visualizer_type=="RadViz":
            self.v.RandianViz(self.X_train,self.y_train, number_of_features)

    def machine_learning(self):
        self._get_train_and_test()

        self.gSH.fit_predict_save(self.X_train,
                                          self.X_test,
                                          self.y_train,
                                          self.testPassengerID,
                                          self.strategy_type)
    def show_result(self):
        self.gSH.show_result()

    def _get_train_and_test(self):
        self.X_train=self.all_data[:self.number_of_train]
        self.X_test=self.all_data[self.number_of_train:]

class D(O):

    def __init__(self, train, test):
        super().__init__(train, test)

    @staticmethod
    def fill_arrays():
        a1 = np.array([1, 2, 3, 4, 5])
        a2 = np.zeros(10000)

        np.append(a1, 6)
        np.append(a1, 7)
        np.append(a1, 8)

        a2[0] = 1
        a2[1] = 2

        for i in range(0,len(a2)):
            a2[i] = 1

        return a1, a2

    def initialize_arrays(self):
        a1, a2 = self.fill_arrays()
        self.dummy_array1 = a1
        self.dummy_array2 = a2

    def increment1(self, i):
        self.dummy_array1[i] += 1

    def increment2(self, i):
        self.dummy_array2[i] += 1

    def dummy_loop(self, length):
        for i in range(0,length):
            self.increment2(i)

    



if __name__ == "__main__":

    print(os.listdir("data"))

    warnings.filterwarnings('ignore')
    print("Warnings were ignored")

    train = pd.read_csv("data/train.csv")
    test = pd.read_csv("data/test.csv")

    dummy=D(train, test)

    dummy.information()

    dummy.preprocessing('strategy1')
    
    dummy.visualize("RadViz", None)

    dummy.machine_learning()

    dummy.show_result()

    dummy.initialize_arrays()
    dummy.increment1(0)
    dummy.increment1(0)
    dummy.initialize_arrays()
    dummy.increment2(1)
    dummy.increment2(2)
    dummy.dummy_loop(100)
