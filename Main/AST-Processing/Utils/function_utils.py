# <--- functions to process inputs and outputs of call nodes --->
"""
We use a heuristic approach following the AST grammar (https://docs.python.org/3/library/ast.html#abstract-grammar).
Function calls follow the pattern: `id.functionName(args)`. Depending on the function name, id and args may be input or output.
    We consider a set amount of function names for which we explicitly declare how to handle id and args.
    This declaration can also be used recursively, e.g., when args itself is another function call.
"""

from . import attribute_utils
import ast

def is_file(name):
    """
    Check if constant value is file.
    """
    if isinstance(name, str):
        return name.endswith(('.csv', '.png'))
    return False

def build_handler(name, vars, deps):
    """
    Build dictionary for functionDef.
    """
    handler = dict()
    handler["name"] = name
    handler["vars"] = vars
    handler["deps"] = deps
    return handler

def get_func_caller(callNode):
    """
    Find the name of the object that called a function.
    """
    id = ""

    child = callNode.func
    # either name
    if isinstance(child, ast.Name):
        id = ""
    # or lowest attribute
    else:     
        id = attribute_utils.get_attr_from_param(child)
    return id

def get_glob_name(arg):
    """
    Find global argument name.
    """
    # is attribute
    for node in ast.walk(arg):
        if isinstance(node, ast.Name) and node.id == "globals" and isinstance(node.parent, ast.Attribute):
            return node.id + "." + node.parent.attr
    return None

def get_elems(arg):
    """
    Function argument may consist of multiple variable names. 
    Here, we collect them from Name, Constant, Attribute, and Tuple nodes.
    """
    if isinstance(arg, ast.Name):
        return [arg.id]
    elif isinstance(arg, ast.Constant) and is_file(arg.value):
        return [arg.value]
    elif isinstance(arg, ast.Attribute):
        name = get_glob_name(arg)
        if name is not None:
            return [name]
    elif isinstance(arg, ast.Tuple):
        elems = list()
        for subarg in arg.elts:
            elems += get_elems(subarg)
        return elems
    return []

def get_args(node):
    """
    Find all argument names passed to function.
    """
    arglist = list()
    for arg in node.args:
        elems = get_elems(arg)
        arglist += elems

    return arglist

def process_call_node(node, functionNames):
    """
    Get function name, id and args of call node.
    """
    # ToDo: recursive for when arguments are not simple
    pair = []
    args = []
    flag = False

    # get function name
    try:
        pair.append(node.func.attr)
    except:
        try:
            # self-defined functions have ids
            pair.append(node.func.id)
            if node.func.id in functionNames:
                flag = True
        except:
            pair.append("")

    id = get_func_caller(node)
    pair.append(id)

    # get potential filenames and variable names used as arguments
    args = get_args(node)    
    pair.append(args)

    return pair, flag

def get_ins_outs(funcName, functionNames, functionHandlers):
    """
    Gain input, output, and dependencies from self-defined functions by checking function handler.
    """
    ins = list()
    outs = list()
    # find funcName index
    index = functionNames.index(funcName)
    vars = functionHandlers[index]["vars"]
    deps = functionHandlers[index]["deps"]
    for var in vars:
        if var[1] == "Load":
            ins.append(var[0])
        if var[1] == "Store":
            outs.append(var[0])
    return ins, outs, deps

def decide_functions(functions, functionNames, functionHandlers):
    """
    Decide whether id and args are input or output depending on function name.
    """
    ins = list()
    outs = list()
    deps = list()

    for item in functions:
        funcName = item[0]
        id = [item[1]]
        args = item[2]

        # <--- self defined functions --->
        if funcName in functionNames:
            ins, outs, deps = get_ins_outs(funcName, functionNames, functionHandlers)

        # <--- pandas functions --->
        if funcName == "read_csv":
            ins += args
        if funcName == "to_csv":
            ins += id
            outs += args
        if funcName == "apply":
            ins += id
            outs += id
        if funcName == "DataFrame":
            ins += args
        if funcName == "iterrows":
            ins += id
        if funcName == "corr":
            ins += id
        
        # <--- seaborn functions --->
        if funcName == "heatmap":
            ins += args

        # <--- matplotlib functions --->
        if funcName == "savefig":
            outs += args

        # <--- numpy functions --->
        if funcName == "arange":
            outs += args
        if id[0] == "np" and funcName == "append":
            outs += args

        # <--- sklearn functions --->
        if funcName == "fit":
            outs += id

        # <--- python functions --->
        if funcName == "range":
            ins += args
        if funcName == "append":
            outs += id
    
    return ins, outs, deps