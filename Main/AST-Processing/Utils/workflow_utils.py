# <--- functions to edit and save text for rule and script creation --->

from . import function_utils

def add_indents(s):
    """ 
    Adds tab space in front of each line in string s. 
    """
    s2 = ""
    for line in s.splitlines():
        s2 = s2 + "    " + line + "\n"
    return s2

# <--- Serialization --->

ser_string = """with open({output}, 'wb') as out_file:
    pickle.dump({rawOutput}, out_file)\n"""

deser_string = """with open({input}, 'rb') as in_file:
    {rawInput} = pickle.load(in_file)\n"""

def names_to_serialized(names):
    """
    Creates filename for each variable.
    """
    serNames = list()
    for name in names:
        if function_utils.is_file(name):
            serNames.append("data/" + name)
        else:
            serNames.append("data/" + name + ".pkl")
    return serNames

# <--- Rule creation --->

def create_rule(dictionary):
    """
    Build Snakemake rule.
    """
    ruleName = "rule_" + str(dictionary["id"])
    ruleText = ""
    inputs = [i[0] for i in dictionary["input"]]
    outputs = [o[0] for o in dictionary["output"]]
    inputs = str(names_to_serialized(inputs))[1:-1]
    outputs = str(names_to_serialized(outputs))[1:-1]
    script = "../scripts/{}.py".format(ruleName)
    
    ruleText = "rule " + ruleName + ":\n"
    ruleText = ruleText + "    " + "input: " + inputs + "\n"
    ruleText = ruleText + "    " + "output: " + outputs + "\n"
    ruleText = ruleText + "    " + "script: " + "'" + script + "'\n\n"
    
    return ruleText

# <--- Script creation --->

def create_main_beginning(dictionary):
    """ 
    Prepares core section of script. 
    """
    text = "\nif __name__ == '__main__':\n\n"
    return text

def prep_names(data):
    """
    Prepare inputs/outputs for script creation.
    """
    names = list()
    files = list()
    for i in range(0, len(data)):
        inputName = data[i][0]
        if function_utils.is_file(inputName):
            files.append((inputName,i))
        else:
            names.append((inputName, i))
    return names, files

def deserialize_inputs(dictionary):
    """
    Add pickle deserialization for all input variables.
    """    
    deserializations = ""
    names, files = prep_names(dictionary["input"])
    for pair in names:
        deserializations += deser_string.format(input='snakemake.input[{}]'.format(pair[1]), rawInput=pair[0])
    return deserializations, files

def serialize_outputs(dictionary):
    """
    Add pickle serialization for all output variables.
    """    
    serializations = ""
    names, files = prep_names(dictionary["output"])

    for pair in names:
        serializations += ser_string.format(output='snakemake.output[{}]'.format(pair[1]), rawOutput=pair[0])
    return serializations, files

def file_replaced_code(code, infiles, outfiles):
    """
    Replace input/output files in code.
    """
    for f in infiles:
        code = code.replace('\"' + f[0] + '\"', 'snakemake.input[{}]'.format(f[1]))
        code = code.replace('\'' + f[0] + '\'', 'snakemake.input[{}]'.format(f[1]))
    for f in outfiles:
        code = code.replace('\"' + f[0] + '\"', 'snakemake.output[{}]'.format(f[1]))
        code = code.replace('\'' + f[0] + '\'', 'snakemake.output[{}]'.format(f[1]))
    return code

def create_main_content(dictionary):
    """
    Builds inner main call of script.
    """
    text1, files1 = deserialize_inputs(dictionary)
    text2, files2= serialize_outputs(dictionary)
    text3 = file_replaced_code(dictionary["code"], files1, files2)
    text = text1 + "\n" + text3 + "\n" + text2
    text = add_indents(text)
    return text

# <--- functionDef tools --->

def get_func_deps(name, handlers):
    """
    Finds right dependencies for functionDef.
    """
    for h in handlers:
        if h["name"] == name:
            return set(h["deps"])
    return set()