# <--- functions to iterate and extract information from AST Attribute nodes --->

import ast

def prepare_parents(tree):
    """
    Annotate each AST node with parent node. 
    """
    for node in ast.walk(tree):
        for child in ast.iter_child_nodes(node):
            child.parent = node
    return tree

def get_global_var(nameNode):
    """
    Get name and context of Attribute node.
    """
    # context may be hidden in parent Subscript node
    if isinstance(nameNode.parent.parent, ast.Subscript):
        context = type(nameNode.parent.parent.ctx).__name__
    else:
        context = type(nameNode.parent.ctx).__name__
    return [nameNode.id + "." + nameNode.parent.attr, context]

def find_attribute_variable(nameNode):
    """
    Find Attribute node that represents variable name and not function name.
    """
    # parent is attribute, but might be a function name
    finalNode = nameNode
    while isinstance(finalNode.parent, ast.Attribute) and (not isinstance(finalNode.parent.parent, ast.Call)):
        finalNode = finalNode.parent
    return finalNode

def get_full_attribute(attrNode):
    """
    Find global variable name in Attribute node.
    """
    name = list()
    name.append(attrNode.attr)
    
    child = attrNode.value
    while isinstance(child, ast.Attribute):
        name.append(child.attr)
        child = child.value
        
    name.append(child.id)
    name.reverse()
    
    context = type(attrNode.ctx).__name__
    fullName = ".".join(name)
    if fullName.startswith("globals"):
        return [fullName, context]
    else:
        return [name[0], context]

def divide_attribute_node(nameNode):
    """
    Find potential parent Attribute node name to gain full variable name.
    """
    node = find_attribute_variable(nameNode)
    if isinstance(node, ast.Name):
        return [node.id, type(nameNode.ctx).__name__]
    else:
        return get_full_attribute(node)

def get_attr_from_param(arg):
    """
    Gain full variable name from function argument.
    """
    for node in ast.walk(arg):
        if isinstance(node, ast.Name):
            return divide_attribute_node(node)[0]
    return ""