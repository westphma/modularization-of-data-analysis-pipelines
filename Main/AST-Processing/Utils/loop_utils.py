# <--- functions to extract input, output and dependencies from loops --->

import ast

def get_head_vars(loopArtefact):
    """
    Collect variables created within loop head. 
    """
    vars = list()
    for node in ast.walk(loopArtefact.target):
        if isinstance(node, ast.Name):
            vars.append(node.id)
    return vars

def get_head_origin(loopArtefact):
    """
    Collect objects we are iterating over.
    """
    origins = list()
    iternodes = iter(ast.walk(loopArtefact.iter))
    for node in iternodes:
        if isinstance(node, ast.Name):
            origins.append([node.id, "Store"])
    return origins