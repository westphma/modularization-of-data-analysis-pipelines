# <--- functions to take user input arguments for modularization --->

import argparse

def setup_parser():
    """
    Argparsing for modularization parameters.
    """
    parser = argparse.ArgumentParser(description="Parameter tuning for modularization")
    parser.add_argument('-f', default=None, action='store', help="Specify json file from which graph should be built.")
    parser.add_argument('-t', default="thorough", choices=['topological', 'thorough'], action='store', help="Specify type of modularization algorithm.")
    parser.add_argument('-i', default=1000, action='store', type=int, help="Specify maximum number of iterations.")
    parser.add_argument('-w', default=[1,1,1,1], nargs='+', type=int, action='store', help="Specify 4 non-negative weights to be used for modularization. Example: -w 1 1 1 1")
    parser.add_argument('-LOC', default=-1, action='store', type=int, help="Specify non-negative limit for lines of code per final node.")
    parser.add_argument('-RUN', default=-1, action='store', type=int, help="Specify non-negative limit for runtime per final node.")
    parser.add_argument('-DATA', default=-1, action='store', type=int, help="Specify non-negative limit for inputs/outputs per final node.")
    return parser

def setup_args(parser, sysargs):
    """
    Process system call arguments.
    """
    rawArgs = list()
    if len(sysargs) > 2:
        rawArgs = sysargs[2:]
    return parser.parse_args(rawArgs)