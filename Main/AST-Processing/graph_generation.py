from operator_annotation import AnnotatedOperators
import sys
import networkx as nx
from networkx.drawing.nx_agraph import to_agraph
import pygraphviz as pgv
import copy
from Utils.file_utils import save_dict_to_file
from Utils.rename_utils import rename_code, add_code


class OperatorGraph(AnnotatedOperators):
    """
    OperatorGraph extends AnnotatedOperators with graph creation functionality. 
    Operators are no longer just represented by dictionaries, but also by graph nodes within a networkx digraph.
    """

    # <--- functions for initialising OperatorGraph --->

    # build dictionaries using operator_annotation functions
    def build_annotated_operators(self):
        print("> Annotating operators with dynamic information...")
        self.create_runtime_file()
        self.create_datatype_file()
        self.create_datasize_file()
        self.run_annotator()
        self.collect_runtimes()
        self.collect_datatypes()
        self.collect_datasizes()
        self.discard_special_types()

    def __init__(self, filename):
        super().__init__(filename)
        self.build_annotated_operators()
        self.sticky_artefacts()
        self.relevantDictionaries = self.stickyArtefacts
        self.renamedDictionaries = copy.deepcopy(self.relevantDictionaries)
        self.finalDictionaries = list()
        self.Graph = nx.DiGraph()

    # <--- functions for displaying operator information --->

    def display_graph_dictionaries(self, kind = "renamed"):
        if kind == "renamed":
            nodes = self.renamedDictionaries
        else:
            nodes = self.finalDictionaries
        for node in nodes:
            text = """{{\nid: {id}\ntype: {type}\ninput: {input}\noutput: {output}\ndependencies: {dependencies}\ncode: \n{code}\nruntime: {runtime}\n}}\n"""
            print(text.format(id=node["id"], type=node["type"], input=node["input"], output=node["output"], \
                dependencies=node["dependencies"], code=node["code"], runtime=node["runtime"]))

    def display_graph(self, filename):
        """
        Saves graph as image.
        """
        A = to_agraph(self.Graph)
        A.layout('dot')
        A.draw(filename)

    # <--- rename variables to display data provenance --->)

    def rename_dictionaries(self):
        """ 
        Iterates over dictionaries and changes variable names to track data changing. 
        Variable names are replaced in code heuristically, but there is no guarantee the replacement
        is accurate! Manual corrections have to be made in in the respective JSON file.
        """
        varTracker = dict()
        for d in self.renamedDictionaries:
            replacements = list()
            for i in d["input"]:
                inputName = i[0]
                # check if input references data that has already been changed previously
                if inputName in varTracker:
                    if varTracker[inputName] != 0:
                        i[0] = inputName + "_" + str(varTracker[inputName])
                        # replace in code
                        replacements.append([inputName, i[0]])
            for o in d["output"]:
                outputName = o[0]
                if outputName in varTracker:
                    varTracker[outputName] += 1
                    o[0] = o[0] + "_" + str(varTracker[o[0]])
                    # replace in code
                    d["code"] = add_code(d["code"], outputName, o[0])
                else:
                    varTracker[outputName] = 0
            for pair in replacements:
                d["code"] = rename_code(d["code"], pair[0], pair[1])

    # <--- build networkx graph --->

    def build_nodes(self):
        """ 
        Add nodes to graph. Each node saves a dictionary it represents. 
        """
        for d in self.renamedDictionaries:
            self.Graph.add_nodes_from([(d["id"], d)])

    @staticmethod
    def add_label(edgeLabels, edge, data):
        """
        Adds shared data name to edge labels.
        """
        if edge in edgeLabels.keys():
            edgeLabels[edge] += ", " + data
        else:
            edgeLabels[edge] = data
        return edgeLabels    

    def find_edges(self):
        """ 
        Match inputs and outputs of renamed dictionaries to find edges.
        """
        edges = list()
        edgelabels = dict()
        for d in self.renamedDictionaries:
            # search for outgoing edges of operator
            for output in d["output"]:
                for d2 in self.renamedDictionaries:
                    for input in d2["input"]:
                        # add edge if data is passed between two dictionaries
                        if output[0] == input[0]:
                            edge = (d["id"],d2["id"])
                            edges.append(edge)
                            edgelabels = self.add_label(edgelabels, edge, output[0])
                            break
        return edges, edgelabels

    def add_labeled_edge(self, edge, edgelabels):
        label = edgelabels[edge]
        self.Graph.add_edges_from([edge], label=label)

    def build_edges(self):
        """ 
        Adds edges to graph. 
        """
        edges, edgelabels = self.find_edges()
        for edge in edges:
            self.add_labeled_edge(edge, edgelabels)

    def build_graph(self):
        """ 
        Calls node and edge collection. 
        """
        self.build_nodes()
        self.build_edges()
    
    # <--- save dictionary to file --->
    
    def save_renamed_dictionaries(self):
        """
        Saves renamed dictionaries to flat and indented file format.
        """
        file = "renamed_dictionaries.json"
        data = self.renamedDictionaries
        save_dict_to_file(data,file)

# <--- Main --->

def main():
    print("<<< COLLECTION OF OPERATORS GRAPH >>>\n\n")
    ops = OperatorGraph(sys.argv[1])

    #print("\n<--- Renamed dictionaries over relevant code artefacts --->\n")
    ops.rename_dictionaries()
    ops.save_renamed_dictionaries()
    ops.build_graph()
    print("\nSaving graph to Graphs/startingGraph.png...\n")
    ops.display_graph("Graphs/startingGraph.png")

if __name__ == "__main__":
    main()