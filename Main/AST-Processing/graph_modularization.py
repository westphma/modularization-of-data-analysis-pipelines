from graph_generation import OperatorGraph
from Utils.modularization_utils import sum_run, sum_size, to_child, get_node_LOC, get_needed_inputs, get_needed_outputs, merge_nodes_real, find_max_sim
from Utils.topology_utils import find_max_sim_top, get_order, update_order
from Utils.file_utils import get_dictionaries_from_file
from Utils.param_utils import setup_parser, setup_args
import sys
import networkx as nx
from networkx.drawing.nx_agraph import to_agraph
import argparse

class Modularizer(OperatorGraph):
    """
    Modularizer extends OperatorGraph with graph modularization functionality.
    """

    # <--- functions for initialising Modularizer --->

    # build graph using graph_generation functions
    def build_operator_graph(self):
        print("> Building operator graph...")
        self.rename_dictionaries()
        self.save_renamed_dictionaries()
        self.build_graph()
        self.display_graph("Graphs/startingGraph.png")

    # get graph runtime normalization factor
    def get_run_norm(self):
        self.maxRuntime = sum_run(self.Graph)

    # get graph datasize normalization factor
    def get_size_norm(self):
        self.maxDatasize = sum_size(self.Graph)

    # find weak components of nodes in graph
    def extract_components(self):
        return [c for c in sorted(nx.weakly_connected_components(self.modGraph), key=len, reverse=True)]

    # initialize modularization params according to args
    def process_args(self, args):
        weights = args.w
        if len(weights) != 4:
            self.weights = [1,1,1,1]
        else:
            self.weights = weights
        self.modType = args.t
        self.maxIters = args.i
        self.RUNLim = args.RUN
        self.LOCLim = args.LOC
        self.DATALim = args.DATA

    # decide whether to build from scratch or file
    def init_graph(self, filename, jsonname):
        if jsonname is None:
            super().__init__(filename)
            self.build_operator_graph()
        else:
            try:
                self.renamedDictionaries = get_dictionaries_from_file(jsonname)
            except:
                sys.exit("JSON file does not exist.")
            self.build_graph()
            self.display_graph("Graphs/startingGraph.png")

    def __init__(self, filename, args):
        self.process_args(args)
        self.Graph = nx.DiGraph()
        self.finalDictionaries = list()
        self.init_graph(filename, args.f)
        self.modGraph = self.Graph.copy()
        self.maxRuntime = None
        self.maxDatasize = None
        self.get_run_norm()
        self.get_size_norm()

    # <--- display final graph --->

    def display_final_graph(self, filename):
        A = to_agraph(self.modGraph)
        A.layout('dot')
        A.draw(filename)

    # <--- Graph updating after merge --->

    def redirect_edges(self, a, b):
        """
        Merging node b into a: we have to redirect all in-/out-edges of b to a.
        """
        # redirect b ingoing edges
        preds = list(self.modGraph.predecessors(b))
        for pred in preds:
            if pred != a:
                self.modGraph.add_edge(pred,a)
            
        # redirect b outgoing edges
        outs = list(self.modGraph.successors(b))
        for out in outs:
            if out != a:
                self.modGraph.add_edge(a,out)

    def update_graph(self, a, b, node):
        """
        Merge nodes a and b in Graph with node.
        """
        # replaces node a with merged node
        attrs = {a: node}
        nx.set_node_attributes(self.modGraph, attrs)
        # redirects edges from b to a
        self.redirect_edges(a, b)
        # deletes b
        self.modGraph.remove_node(b)

    # <--- Similarity Calculation --->

    def get_IO_dependency(self, a, b):
        """
        Find size of shared data between nodes a and b.
        """
        datasize = 0
        # if a is not child of b or vice versa, they do not shared data
        a, b = to_child(self.modGraph, a, b)
        if a is None:
            return 0
        # else determine IO size
        nodeA = self.modGraph.nodes[a]
        nodeB = self.modGraph.nodes[b]
        for o in nodeA["output"]:
            for i in nodeB["input"]:
                if o == i:
                    datasize += i[2]
        return datasize

    def get_IO_similarity(self, a, b, weight):
        """
        Return normalized, weighted IO similarity.
        """
        datasize = self.get_IO_dependency(a, b)  
        # norm datasize
        datasize = datasize/self.maxDatasize
        # sim func
        similarity = weight*datasize
        return similarity

    def get_II_dependency(self, a, b):
        """
        Find size of similar input data between nodes a and b.
        """
        datasize = 0
        # determine II size
        nodeA = self.modGraph.nodes[a]
        nodeB = self.modGraph.nodes[b]
        for iA in nodeA["input"]:
            for iB in nodeB["input"]:
                if iA == iB:
                    datasize += iB[2]
        return datasize

    def get_II_similarity(self, a, b, weight):
        """
        Return normalized, weighted II similarity.
        """
        # calculate common datasize
        datasize = self.get_II_dependency(a, b)
        # norm datasize
        datasize = datasize/self.maxDatasize
        # sim func
        similarity = weight*datasize
        return similarity

    def impact_parallelism(self, a, b):
        """
        Finds out if merging a and b leads to other nodes having to wait longer before being ready.
        """
        # impact is either 0 or sum of runtimes of a and b
        commonRuntime = self.modGraph.nodes[a]["runtime"] + self.modGraph.nodes[b]["runtime"]
        
        # Case 1: Parallelism is impacted if a is not child of b or vice versa
        a, b = to_child(self.modGraph, a, b)
        if a is None:
            return commonRuntime

        # --- Case 2: b is child of a ---
        # Case 2.1: Parallelism is impacted if a has children apart from b 
        # and those children are not descendants of b
        successors = list(self.modGraph.successors(a))
        numChild = len(successors)
        if numChild > 1:
            descendantsB = list(nx.descendants(self.modGraph, b))
            otherChildren = [child for child in successors if child not in descendantsB and child != b]
            if len(otherChildren) != 0:
                return commonRuntime
        # Case 2.2: Parallelism is impacted if b has parents apart from a 
        # and those parents are not ascendants of a
        predecessors = list(self.modGraph.predecessors(b))
        numParents = len(predecessors)
        if numParents > 1:
            ascendantsA = list(nx.ancestors(self.modGraph, a))
            otherParents = [parent for parent in predecessors if parent not in ascendantsA and parent != a]
            if len(otherParents) != 0:
                return commonRuntime
        # otherwise no impact
        return 0

    def get_runtime_similarity(self, a, b, weight):
        """
        Return normalized, weighted RUN similarity.
        """
        runtime = self.impact_parallelism( a, b)
        # norm runtime
        runtime = runtime/self.maxRuntime
        # sim func
        similarity = weight*(1-runtime)
        return similarity

    def get_library_dependency(self, a, b):
        """
        Find number of common/uncommon library dependencies between a and b.
        """
        nodeA = self.modGraph.nodes[a]
        nodeB = self.modGraph.nodes[b]
        
        commonElems = list(set(nodeA["dependencies"]).intersection(set(nodeB["dependencies"])))
        numCommon = len(commonElems)
        allElems = set(nodeA["dependencies"]) | set(nodeB["dependencies"])
        numAll = len(allElems)

        return numCommon, numAll

    def get_library_similarity(self, a, b, weight):
        """
        Return normalized, weighted LIB similarity.
        """
        numCommon, numAll = self.get_library_dependency(a, b)
        # if no libraries are needed at all, we just set the similarity to zero
        if numAll == 0:
            return 0
        else:
            similarity = weight*(numCommon/numAll)
        return similarity

    def similarity(self, a, b):
        """
        Sum all subsimilarity functions for a pair of nodes (a,b).
        """
        IO_sim= self.get_IO_similarity(a, b, self.weights[0])
        II_sim = self.get_II_similarity(a, b, self.weights[1])
        RUN_sim = self.get_runtime_similarity(a, b, self.weights[2])
        LIB_sim = self.get_library_similarity(a, b, self.weights[3])
        
        similarity = (IO_sim + II_sim + RUN_sim + LIB_sim)/sum(self.weights)
        return similarity

    # <--- Static Metrics --->

    def can_merge(self, a, b):
        """
        Decide whether merging a and b results in a node that exceeds static exclusion criteria.
        """
        nodeA = self.modGraph.nodes[a]
        nodeB = self.modGraph.nodes[b]
        
        run = nodeA["runtime"] + nodeB["runtime"]
        if self.RUNLim != -1 and run > self.RUNLim:
            return False
        LOC = get_node_LOC(nodeA) + get_node_LOC(nodeB)
        if self.LOCLim != -1 and LOC > self.LOCLim:
            return False
        inputs = len(get_needed_inputs(self.modGraph, a, b))
        outputs = len(get_needed_outputs(self.modGraph, a, b))
        if self.DATALim != -1 and (inputs > self.DATALim or outputs > self.DATALim):
            return False
        return True

    # <--- Topological Clustering --->

    def similarity_list_builder(self, topologicalOrder):
        """
        Build list of similarities.
        """
        similarity_list = list()
        # entry i represents pair of nodes at indexes [i, i+1]
        for i in range(0, len(topologicalOrder)-1):
            a = topologicalOrder[i]
            b = topologicalOrder[i+1]
            similarity_list.append(self.similarity(a, b))
        return similarity_list

    def update_similarity_top(self, simMatrix, simIndex, topologicalOrder):    
        """
        Set pairs that were merged to -1.
        """
        # update previously highest similarity to -1 upon merge
        simMatrix[simIndex] = -1
        # propagate -1 upon seeing it in a neighbour
        for i in range(0, len(simMatrix)):
            if simMatrix[i] != -1:
                a = topologicalOrder[i]
                b = topologicalOrder[i+1]
                sim = self.similarity(a, b)
                simMatrix[i] = sim           
        return simMatrix

    def next_merge_top(self, iteration, topologicalOrder, simMatrix):
        """
        Returns Graph, topologicalOrder, simMatrix, limit after each iteration. 
        """
        
        # get next best similarity
        simIndex, sim = find_max_sim_top(simMatrix)
        if simIndex == -1:
            print("\nNo more sims")
            return topologicalOrder, simMatrix, -1,
        
        # get two nodes supposed to merge
        a = topologicalOrder[simIndex]
        b = topologicalOrder[simIndex+1]
        nodeA = self.modGraph.nodes[a]
        nodeB = self.modGraph.nodes[b]
        
        # decide if acceptable
        mergeable = self.can_merge(a, b)
        if mergeable:
            needed_input = get_needed_inputs(self.modGraph, a, b)
            needed_output = get_needed_outputs(self.modGraph, a, b)
            node = merge_nodes_real(nodeA, nodeB, needed_input, needed_output)
            
            # update topologicalOrder
            topologicalOrder = update_order(topologicalOrder, simIndex, a)
            # update graph
            self.update_graph(a, b, node)
            # update similarity
            simMatrix = self.update_similarity_top(simMatrix, simIndex, topologicalOrder)
        else:
            simMatrix[simIndex] = -1
            return self.next_merge_top(iteration, topologicalOrder, simMatrix)
        return topologicalOrder, simMatrix, sim

    def modularize_graph_top(self, maxIter, topologicalOrder, simMatrix):
        """
        Run maxIter iterations of topological clustering.
        """
        current_best_sim = 1
        iterCount = 0
        while(current_best_sim >= 0 and iterCount <= maxIter):
            iterCount += 1
            topologicalOrder, simMatrix, sim = self.next_merge_top(iterCount, topologicalOrder, simMatrix)
            current_best_sim = sim
        print("Modularization stopped. ")

    def run_modularization_top(self):
        """
        Initializes topological ordering and similarities and runs topological clustering.
        """
        topology = get_order(self.renamedDictionaries)
        simMatrix = self.similarity_list_builder(topology)
        self.modularize_graph_top(self.maxIters, topology, simMatrix)

    # <--- Thorough Clustering --->

    def find_non_mergeable(self, node):
        """
        Find all nodes y, which are reachable from node x using a path (x,...,y) of length >= 3.
        """
        non_mergeable = set()
        # for each other node find longest simple path
        for otherNode in self.modGraph.nodes:
            simplePaths = list(nx.all_simple_paths(self.modGraph, node, otherNode))
            if len(simplePaths) == 0:
                continue 
            longestPath = max(simplePaths, key=lambda x: len(x))
            # if at least one node between the two, can't merge
            if len(longestPath) >= 3:
                non_mergeable.add(otherNode)
        return non_mergeable

    def cycle_matrix_builder(self):
        """
        Creates matrix with entry [i,j] == 1 indicating nodes i and j can be merged without cycles.
        """
        num_nodes = len(self.modGraph.nodes)
        matrix = [[0 for x in range(num_nodes)] for y in range(num_nodes)]
        for i in range(0, num_nodes):
            node = list(self.modGraph.nodes)[i]
            non_mergeables = self.find_non_mergeable(node)
            for j in range(0, num_nodes):
                otherNode = list(self.modGraph.nodes)[j]
                if j > i and otherNode not in non_mergeables:
                    matrix[i][j] = 1
                    matrix[j][i] = 1
        return matrix

    def cycle_matrix_updater(self, cycleMatrix, nodeIDs, mergedNodes):
        """
        Reconsider cyclic merges for all non-merged nodes.
        """
        num_nodes = len(cycleMatrix)
        cycleMatrix = [[1 for x in range(num_nodes)] for y in range(num_nodes)]
        for i in range(0, num_nodes):
            node = nodeIDs[i]
            if node not in mergedNodes:
                non_mergeables = self.find_non_mergeable(node)
                for j in range(0, num_nodes):
                    otherNode = nodeIDs[j]
                    if otherNode in non_mergeables:
                        cycleMatrix[i][j] = 0
                        cycleMatrix[j][i] = 0
        return cycleMatrix

    def similarity_matrix_builder(self, cycle_matrix):
        """
        Calculates the similarity between each pair of nodes.
        Similarity is -1 if two nodes cannot be merged due to acyclic criteria.
        """
        num_nodes = len(self.modGraph.nodes)
        similarity_matrix = [[-1 for x in range(num_nodes)] for y in range(num_nodes)]
        # calculate similarity for each 1 in cycle matrix
        for i in range(0, num_nodes):
            for j in range(0, num_nodes):
                if cycle_matrix[i][j] == 1:
                    a = list(self.modGraph.nodes)[i]
                    b = list(self.modGraph.nodes)[j]
                    sim = self.similarity(a, b)
                    similarity_matrix[i][j] = sim
        return similarity_matrix

    def matrices_updater(self, nodeIDs, mergedNodes, i, j, simMatrix, cycleMatrix):
        """
        Update similarity and cycle matrix after merging nodes i and j.
        """
        # set j row and column to 'unavailable' == -2
        for i in range(0,len(simMatrix)):
            simMatrix[j][i] = -2
            simMatrix[i][j] = -2
        # redo cycle analysis
        cycleMatrix = self.cycle_matrix_updater(cycleMatrix, nodeIDs, mergedNodes)
        # update all entries != -2
        for i in range(0, len(simMatrix)):
            node = nodeIDs[i]
            if node not in mergedNodes: 
                for j in range(0, len(simMatrix)):
                    otherNode = nodeIDs[j]
                    if j!=i and otherNode not in mergedNodes and simMatrix[i][j] != -2:
                        # no cycles -> calculate similarity
                        if cycleMatrix[i][j] == 1:
                            simMatrix[i][j] = self.similarity(node, otherNode)
                        # otherwise -> mark as cycle
                        else:
                            simMatrix[i][j] = -1 
        return simMatrix, cycleMatrix

    def decide_index(self, nodeIDs, i, j):
        """
        Return i and j where node at index j is child of node at index i.
        """
        a = nodeIDs[i]
        b = nodeIDs[j]
        if a in self.modGraph.successors(b):
            return j,i
        else: 
            return i,j
        

    def next_merge(self, nodeIDs, mergedNodes, simMatrix, cycleMatrix):
        """
        Returns Graph, matrices, merged nodes after each iteration. 
        """
        # get next best similarity
        sim, simI, simJ = find_max_sim(simMatrix)    
        if simI <= -1:
            print("\nNo more sims")
            return simMatrix, cycleMatrix, mergedNodes, -1
        simI, simJ = self.decide_index(nodeIDs, simI, simJ)
        # get two nodes supposed to merge
        a = nodeIDs[simI]
        b = nodeIDs[simJ]
        nodeA = self.modGraph.nodes[a]
        nodeB = self.modGraph.nodes[b]
        # decide if acceptable
        mergeable = self.can_merge(a, b)
        # if yes, update graph update simMatrix
        if mergeable:
            needed_input = get_needed_inputs(self.modGraph, a, b)
            needed_output = get_needed_outputs(self.modGraph, a, b)
            node = merge_nodes_real(nodeA, nodeB, needed_input, needed_output)
            # update graph
            self.update_graph(a, b, node)
            mergedNodes = mergedNodes + [b]
            # update similarity
            simMatrix, cycleMatrix =  self.matrices_updater(nodeIDs, mergedNodes, simI, simJ, simMatrix, cycleMatrix)
        # if not, repeat with similarity limit
        else:
            simMatrix[simI][simJ] = -2
            simMatrix[simJ][simI] = -2
            return self.next_merge(nodeIDs, mergedNodes, simMatrix, cycleMatrix)
        
        return simMatrix, cycleMatrix, mergedNodes, sim

    def modularize_graph(self, maxIter, nodeIDs, mergedNodes, simMatrix, cycleMatrix):
        """
        Run certain number of clustering steps.
        """
        current_best_sim = 1
        iterCount = 0
        while(current_best_sim >= 0 and iterCount <= maxIter):
            iterCount += 1
            simMatrix, cycleMatrix, mergedNodes, sim = self.next_merge(nodeIDs, mergedNodes, simMatrix, cycleMatrix)
            current_best_sim = sim
        print("Modularization stopped. ")

    def run_modularization(self):
        """
        Initializes cycleMatrix and simMatrix and runs thorough clustering.
        """
        cycleMatrix = self.cycle_matrix_builder()
        simMatrix = self.similarity_matrix_builder(cycleMatrix)
        mergedNodes = list()
        nodeIDs = list(self.modGraph.nodes)
        self.modularize_graph(self.maxIters, nodeIDs, mergedNodes, simMatrix, cycleMatrix)

    # <--- rebuild dictionaries from graph --->
    
    def extract_dictionaries(self):
        for i in range(0, len(self.modGraph.nodes)):
            node = list(self.modGraph.nodes)[i]
            self.finalDictionaries.append(self.modGraph.nodes[node])

# <--- Main --->

def main():
    print("<<< MODULARIZATION OF OPERATOR GRAPH >>>\n\n")

    parser = setup_parser()
    args = setup_args(parser, sys.argv)
    ops = Modularizer(sys.argv[1], args)
    
    print("\nMerging nodes...\n")
    if ops.modType == "topological":
        ops.run_modularization_top()
    else:
        ops.run_modularization()

    print("\n<--- Operators after modularization --->\n")
    ops.extract_dictionaries()
    #ops.display_graph_dictionaries("finalDictionaries")
    ops.display_final_graph("Graphs/finalGraph.png")

    components = ops.extract_components()
    print("\nWeak components: ", components)

if __name__ == "__main__":
    main()