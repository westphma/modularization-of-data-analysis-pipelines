from numpy import isin
from core_operator_generation import CoreOperators
from Utils.function_utils import process_call_node, decide_functions, is_file, build_handler
from Utils.loop_utils import get_head_origin, get_head_vars
from Utils.def_utils import get_params, get_returns, get_local_vars
from Utils.modularization_utils import merge_nodes
from Utils.file_utils import save_dict_to_file
from Utils.attribute_utils import prepare_parents, get_global_var
import ast
import sys
import copy

class Operators(CoreOperators):
    """
    Operators extends CoreOperators with more complicated static information collection - including input, output and import dependency extraction.
    We use heuristic approaches to collect this information from special classes in our AST.
    """

    # <--- functions for initialising Operators --->

    # build core dictionaries using core_operator_generation functions
    def build_core_operators(self):
        self.build_dictionaries()
        self.build_relevantDictionaries()

    # get code artefacts, which are defined by the first layer of AST nodes
    # each dictionary represents one artefact
    def get_artefacts(self):
        artefacts = list()
        for artefact in ast.iter_child_nodes(self.tree):
            artefacts.append(artefact)
        return artefacts

    # extract all import module/function names
    def get_imports(self):
        imports = list()
        for artefact in self.artefacts:
            if isinstance(artefact, ast.Import):
                if artefact.names[0].asname is not None:
                    imports.append([artefact.names[0].name, artefact.names[0].asname])
                else:
                    imports.append([artefact.names[0].name])
            if isinstance(artefact, ast.ImportFrom):
                names = [a.name for a in artefact.names]
                asnames = [a.asname for a in artefact.names if a.asname is not None]
                imports.append(names + asnames)
        return imports
    
    # get the names of functions that were defined in source code
    def get_functionNames(self):
        funcNames = list()
        for artefact in self.artefacts:
            if isinstance(artefact, ast.FunctionDef):
                funcNames.append(artefact.name)
        return funcNames

    # get names of special functions like range() or len()
    def get_special_function_names(self):
        for node in ast.walk(self.tree):
            if isinstance(node, ast.Call):
                try:
                    if node.func.id not in self.functionNames:
                        self.specialFunctions.append(node.func.id)
                except:
                    continue

    # get self-defined function handler
    def get_funcDef_vars(self):
        """ Collects variable names and their usage context but sorts out local variables. """
        for defArtefact in [node for node in ast.walk(self.tree) if isinstance(node, ast.FunctionDef)]:
            varNames = get_returns(defArtefact)
            locals = get_local_vars(defArtefact)[0]
            params = get_params(defArtefact)
            for node in ast.walk(defArtefact):
                        if isinstance(node, ast.Name) and node.id not in locals:
                            varNames.append([node.id, type(node.ctx).__name__])
            self.functionHandler.append([params, varNames])

    # extend self-defined function handler with function call handling
    def get_funcDef_funcs(self):
        index = 0
        for node in ast.walk(self.tree):
            if isinstance(node, ast.FunctionDef):
                functions = list()
                locals = get_local_vars(node)
                for child in ast.walk(node):
                    if isinstance(child, ast.Call):
                        functions.append(process_call_node(child, self.functionNames)[0])
                ins, outs = decide_functions(functions, self.functionNames, self.functionHandler)
                for i in [i for i in ins if not i in locals]:
                    self.functionHandler[index][1].append([i,"Load"])
                for o in [o for o in outs if not o in locals]:
                    self.functionHandler[index][1].append([o,"Store"])
                index += 1

    # extract dependencies for self-defined functions
    def redirect_func_imports(self):
        dependencies = list()
        for varList in self.functionHandler:
            for var in varList[1][:]:
                if self.find_import(var[0]) is not None:
                    varList[1].remove(var)
                    dependencies.append(self.find_import(var[0]))
            self.functionDependencies.append(dependencies)

    def __init__(self, filename):
        super().__init__(filename)
        self.build_core_operators()
        self.tree = prepare_parents(self.tree)
        self.artefacts = self.get_artefacts()
        self.imports = self.get_imports()
        self.functionNames = self.get_functionNames()
        self.functionHandler = list()
        self.specialFunctions = list()
        self.get_special_function_names()

    # <--- functions for displaying operator information --->

    def display_artefacts(self):
        for a in self.artefacts:
            print(a)

    def display_imports(self):
        for i in self.imports:
            print(i)

    def display_function_calls(self):
        for node in ast.walk(self.tree):
            if isinstance(node, ast.Call):
                print(process_call_node(node, self.functionNames)[0])

    def display_sticky(self):
        nodes = self.stickyArtefacts

        for node in nodes:
            text = """{{\nid: {id}\ntype: {type}\ninput: {input}\noutput: {output}\ndependencies: {dependencies}\ncode: \n{code}\nruntime: {runtime}\n}}\n"""
            print(text.format(id=node["id"], type=node["type"], input=node["input"], output=node["output"], \
                dependencies=node["dependencies"], code=node["code"], runtime=node["runtime"]))

    def display_handlers(self):
        for handler in self.functionHandler:
            print(handler)

    # <--- collecting inputs, outputs and dependencies from artefacts --->

    # <--- import dependencies --->

    def find_import(self, name):
        """
        Finds index of a module name in list of module names.
        """
        for i in range(0, len(self.imports)):
            if name in self.imports[i]:
                return i
        for i in range(0, len(self.functionNames)):
            if name == self.functionNames[i]:
                return -1 * (i+1)
        return None

    def redirect_import_dependencies(self):
        """
        Redirects module names from "input"/"output" key to "dependencies" key. 
        """
        for d in self.relevantDictionaries:
            for name in d["input"][:]:
                if self.find_import(name) is not None:
                    d["input"].remove(name)
                    d["dependencies"].append(self.find_import(name))
            for name in d["output"][:]:
                if self.find_import(name) is not None:
                    d["output"].remove(name)
                    d["dependencies"].append(self.find_import(name))

    # <--- loop inputs, outputs and dependencies --->

    def get_loop_vars(self, loopArtefact):
        """ 
        Collects variable names and their usage context but sorts out loop head variables. 
        """
        # ToDo: Check if origins needs different context
        headVars = get_head_vars(loopArtefact)
        origins = get_head_origin(loopArtefact)
        vars = list()
        for node in ast.iter_child_nodes(loopArtefact):
            vars += self.collect_varNames(node)

        for var in vars[:]:
            if var[0] in headVars:
                if var[1] == "Load":
                    vars.remove(var)
                elif origins is not None:
                    vars += origins

        return vars

    # <--- functionDef inputs, outputs and dependencies --->

    def collect_IOD_artefact(self, artefact):
        """
        Executes generic IO analysis and function-specific IO analysis for one artefact. 
        """
        ioVarList = self.collect_varNames(artefact)
        ins, outs, deps = self.collect_func_varNames(artefact)

        ioVarList += [[i, "Load"] for i in ins]
        ioVarList += [[o, "Store"] for o in outs]

        return ioVarList, deps
    
    def collect_IOD_artefacts(self, artefacts):
        """
        Iterates content of functionDef and approximates input, output, and dependencies.
        """
        ioVars = list()
        deps = list()
        for artefact in artefacts:
            ioVar, dep = self.collect_IOD_artefact(artefact)
            ioVars += ioVar
            deps += dep
        return ioVars, deps

    def divide_func_vars(self, artefact, approxVars, deps):
        """
        Discards local and overapproximated inputs/outputs from functionDef IO analysis.
        """     
        ioVars = list()

        # redirect dependencies from vars
        depNames = [name[0] for name in approxVars if self.find_import(name[0]) is not None]
        ioVars = [var for var in approxVars if var[0] not in depNames]
        deps += [self.find_import(name) for name in depNames]
        
        # get globals
        globs = [a.name for a in artefact.body if isinstance(a, ast.Global)]
        # only keep globals and files
        ioVars = [var for var in ioVars if (var[0] in globs or var[0].startswith("globals") or is_file(var[0]))]

        return ioVars, deps

    def get_function_handlers(self):
        """
        Prebuild dictionaries of correct inputs, outputs, and dependencies for functionDef artefacts.
        """
        for artefact in self.artefacts:
            if isinstance(artefact, ast.FunctionDef):
                approxVars, deps = self.collect_IOD_artefacts(artefact.body)
                ioVars, deps = self.divide_func_vars(artefact, approxVars, deps)
                handler = build_handler(artefact.name, ioVars, deps)
                self.functionHandler.append(handler)    

    # <--- artefact analysis --->

    def collect_varNames(self, artefact):
        """ 
        Get all variable names and their usage context from an AST node. 
        """
        varNames = list()
        if isinstance(artefact, ast.For):
            return self.get_loop_vars(artefact)
        for node in ast.walk(artefact):
            if isinstance(node, ast.Name):
                # certain global variables are attributes of format "globals.a" for our testcases
                # therefore, the actual name is hidden in an ast.Attribute node
                if node.id == "globals":
                    varNames.append(get_global_var(node))
                varNames.append([node.id, type(node.ctx).__name__])
        # lambda expressions create variables that have no "def" context
        # we have to tweak our analysis to catch lambda variables
        for node in ast.walk(artefact):
            if isinstance(node, ast.Lambda):
                lambdaLoader = [node.args.args[0].arg, "Load"]
                # discard all "use" contexts of lambda variable
                varNames = [v for v in varNames if v != lambdaLoader]
        # recognize subscript nodes
        for node in ast.walk(artefact):
            if isinstance(node, ast.Subscript):
                if isinstance(node.value, ast.Name):
                    varNames.append([node.value.id, "Load"])
                    if type(node.ctx).__name__ == "Store":
                        varNames.append([node.value.id, "Store"])
                elif isinstance(node.value, ast.Attribute):
                    for subnode in ast.walk(node.value):
                        if isinstance(subnode, ast.Name):
                            if subnode.id == "globals":
                                globname = get_global_var(subnode)[0]
                                varNames.append([globname, "Load"])
                                if type(node.ctx).__name__ == "Store":
                                    varNames.append([globname, "Store"])
                
        return varNames      

    def collect_via_context(self, artefact, i):
        """ 
        Sort variable names into dictionary "input" or "output" depending on their usage context. 
        """
        varNames = self.collect_varNames(artefact)
        for var in varNames:
            if var[1] == "Load":
                self.relevantDictionaries[i]["input"].append(var[0])
            elif var[1] == "Store":
                self.relevantDictionaries[i]["output"].append(var[0])

    def collect_func_varNames(self, artefact):
        """
        Get input, output, and dependencies from function calls in artefact.
        """
        # collect functions
        functions = list()
        for node in ast.walk(artefact):
            if isinstance(node, ast.Call):
                functions.append(process_call_node(node, self.functionNames)[0])
        # collect their respective inputs and outputs
        ins, outs, deps = decide_functions(functions, self.functionNames, self.functionHandler)

        # collect dependency if functionName was imported
        for function in functions:
            funcName = function[0]
            dep = self.find_import(funcName)
            if dep is not None:
                deps.append(dep)

        return ins, outs, deps

    def collect_via_function(self, artefact, i):
        """
        Sort variable names into dictionary "input" or "output" depending on their usage context in function calls.
        Some might have already been caught by collect_via_context.
        """

        ins, outs, deps = self.collect_func_varNames(artefact)
        self.relevantDictionaries[i]["input"] += ins
        self.relevantDictionaries[i]["output"] += outs
        self.relevantDictionaries[i]["dependencies"] += deps


    def process_relevant_artefacts(self):
        """ 
        Call input and output collection over all relevant artefacts. 
        """
        for i in range(0, len(self.relevantDictionaries)):
            artefact = self.artefacts[self.relevantDictionaries[i]["id"]]
            self.collect_via_context(artefact, i)
            self.collect_via_function(artefact, i)            

    # <--- remove duplicates from dictionaries --->

    def remove_duplicates(self):
        for d in self.relevantDictionaries:
            d["input"] = list(set(d["input"]))
            d["output"] = list(set(d["output"]))
            d["dependencies"] = list(set(d["dependencies"]))

    # <--- discard function names --->

    def remove_funcNames(self):
        """
        Simple function names may have been caught as input/output wrongfully. Here, we discard them.
        """
        for d in self.relevantDictionaries:
            d["input"] = [i for i in d["input"] if not i in self.specialFunctions]
            d["output"] = [o for o in d["output"] if not o in self.specialFunctions]

    # <--- merge operators with no output --->
    
    def sticky_artefacts(self):
        """
        Heuristically merges no-output artefacts with successor/predecessor artefact.
        """
        self.stickyArtefacts = list()
        tempDict = copy.deepcopy(self.relevantDictionaries)
        for i in range(0, len(tempDict)):
            d = tempDict[i]
            if len(d["output"]) == 0:
                if len(self.stickyArtefacts) != 0:
                    # merge upwards
                    self.stickyArtefacts[-1] = merge_nodes(self.stickyArtefacts[-1], d)
                    continue
                else:
                    # merge downwards
                    if i+1 == len(tempDict):
                        print("Need at least one output artifact!")
                        self.stickyArtefacts = tempDict
                        break
                    tempDict[i+1] = merge_nodes(tempDict[i], tempDict[i+1])
                    continue
            self.stickyArtefacts.append(d)

    def save_sticky_dictionaries(self):
        """
        Saves relevant dictionaries to flat and indented file format.
        """
        file = "sticky_dictionaries.json"
        data = self.stickyArtefacts
        save_dict_to_file(data,file)

# <--- Main --->

def main():
    print("<<< COLLECTION OF OPERATORS USING STATIC AST INFORMATION >>>\n\n")
    ops = Operators(sys.argv[1])
    
    print("\n<--- Artefacts extracted from file {filename} --->\n".format(filename=sys.argv[1]))
    ops.display_artefacts()

    print("\n<--- Imports extracted from AST --->\n")
    ops.display_imports()

    print("\n<--- Function calls extracted from AST --->\n")
    ops.display_function_calls()

    print("\n<--- FunctionHandlers extracted from FunctionDefs --->\n")
    ops.get_function_handlers()
    ops.display_handlers()

    #print("\n<--- Operators as dictionaries over relevant code artefacts --->\n")
    ops.process_relevant_artefacts()
    ops.remove_funcNames()
    ops.redirect_import_dependencies()
    ops.remove_duplicates()

    print("\n<--- Saving dictionaries --->\n")
    ops.save_dictionaries()
    ops.save_relevant_dictionaries()

if __name__ == "__main__":
    main()