import sys
import pickle
import globals
import pandas as pd
from funcs.functions import O__get_all_data

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.O_test = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        globals.O_train = pickle.load(in_file)
    
    globals.O_strategy_type='strategy1'
    
    globals.S_dats=O__get_all_data()
    globals.S_dats_1=globals.S_dats
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.O_strategy_type, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(globals.S_dats_1, out_file)
