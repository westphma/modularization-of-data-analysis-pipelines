import sys
import pickle
import globals
import pandas as pd

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.S_dats_3 = pickle.load(in_file)
    
    globals.S_dats_3 = globals.S_dats_3
    globals.S_dats_4=globals.S_dats_3
    
    globals.S_dats_4['FamilySize'] = globals.S_dats_4['SibSp'] + globals.S_dats_4['Parch'] + 1
    globals.S_dats_5=globals.S_dats_4
    
    globals.S_dats_5['IsAlone'] = 1
    globals.S_dats_6=globals.S_dats_5
    
    globals.S_dats_6.loc[(globals.S_dats_6['FamilySize'] > 1), 'IsAlone'] = 0
    globals.S_dats_7=globals.S_dats_6
    
    globals.S_dats_7['Title'] = globals.S_dats_7['Name'].str.split(", ", expand=True)[1].str.split('.', expand=True)[0]
    globals.S_dats_8=globals.S_dats_7
    
    min_lengtht = 10
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.S_dats_8, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(min_lengtht, out_file)
