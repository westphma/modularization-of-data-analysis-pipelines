import sys
import pickle
import globals
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from yellowbrick.features import RadViz
from funcs.functions import O__get_train_and_test

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.G_clf_and_params_5 = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        globals.O_strategy_type = pickle.load(in_file)
    with open(snakemake.input[2], 'rb') as in_file:
        globals.O_testPassengerID = pickle.load(in_file)
    with open(snakemake.input[3], 'rb') as in_file:
        globals.O_y_train = pickle.load(in_file)
    with open(snakemake.input[4], 'rb') as in_file:
        globals.O_number_of_train = pickle.load(in_file)
    with open(snakemake.input[5], 'rb') as in_file:
        globals.O_all_data_1 = pickle.load(in_file)
    with open(snakemake.input[6], 'rb') as in_file:
        globals.O_X_train = pickle.load(in_file)
    with open(snakemake.input[7], 'rb') as in_file:
        features = pickle.load(in_file)
    
    fig, ax=plt.subplots(1, figsize=(15,12))
    
    radViz=RadViz(classes=['survived', 'not survived'], features=features)
    
    radViz.fit(globals.O_X_train, globals.O_y_train)
    radViz.transform(globals.O_X_train)
    radViz.poof()
    radViz_1=radViz
    
    O__get_train_and_test()
    globals.O_X_test_1=globals.O_X_test
    globals.O_X_train_1=globals.O_X_train
    
    globals.G_X_train=globals.O_X_train_1
    
    globals.G_X_test=globals.O_X_test_1
    
    globals.G_y_train=globals.O_y_train
    
    globals.G_submission_id=globals.O_testPassengerID
    
    globals.G_strategy_type=globals.O_strategy_type
    
    clf_and_params = globals.G_clf_and_params_5
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.G_strategy_type, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(globals.G_submission_id, out_file)
    with open(snakemake.output[2], 'wb') as out_file:
        pickle.dump(globals.G_X_train, out_file)
    with open(snakemake.output[3], 'wb') as out_file:
        pickle.dump(globals.G_y_train, out_file)
    with open(snakemake.output[4], 'wb') as out_file:
        pickle.dump(globals.G_X_test, out_file)
    with open(snakemake.output[5], 'wb') as out_file:
        pickle.dump(clf_and_params, out_file)
