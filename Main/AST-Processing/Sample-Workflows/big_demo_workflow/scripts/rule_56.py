import sys
import pickle
import globals
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.O_all_data = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        params_4 = pickle.load(in_file)
    with open(snakemake.input[2], 'rb') as in_file:
        globals.G_clf_and_params_4 = pickle.load(in_file)
    with open(snakemake.input[3], 'rb') as in_file:
        clf_4 = pickle.load(in_file)
    
    globals.G_clf_and_params_4.append((clf_4, params_4))
    globals.G_clf_and_params_5=globals.G_clf_and_params_4
    
    feature_dtypes=globals.O_all_data.dtypes
    
    missing_values = globals.O_all_data.isnull().sum()
    
    missing_values = missing_values.sort_values(ascending=False)
    missing_values_1=missing_values
    
    globals.I_missing_values = missing_values_1
    print("=" * 50)
    print("{:16} {:16} {:25} {:16}".format("Feature Name".upper(),
                                                                "Data Format".upper(),
                                                                "# of Missing Values".upper(),
                                                                "Samples".upper()))
    
    for feature_name, dtype, missing_value in zip(globals.I_missing_values.index.values,
                                                  feature_dtypes[globals.I_missing_values.index.values],
                                                  globals.I_missing_values.values):
        print("{:18} {:19} {:19} ".format(feature_name, str(dtype), str(missing_value)), end="")
        for v in globals.O_all_data[feature_name].values[:10]:
            print(v, end=",")
        print()
    print("="*50)
    feature_dtypes_1=feature_dtypes
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.G_clf_and_params_5, out_file)
