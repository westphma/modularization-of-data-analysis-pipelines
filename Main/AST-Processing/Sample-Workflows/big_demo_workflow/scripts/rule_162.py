import sys
import pickle
from sklearn.ensemble import RandomForestClassifier
import globals
import pandas as pd
from sklearn.model_selection import GridSearchCV

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.G_X_train = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        globals.G_y_train = pickle.load(in_file)
    with open(snakemake.input[2], 'rb') as in_file:
        clf_and_params = pickle.load(in_file)
    
    results4 = "Saved results"
    print()
    
    clf5 = clf_and_params[4][0]
    
    params5 = clf_and_params[4][1]
    
    globals.G_current_clf_name = clf5.__class__.__name__
    globals.G_current_clf_name_4=globals.G_current_clf_name
    
    grid_search_clf = GridSearchCV(clf5, params5, cv=5)
    grid_search_clf_8=grid_search_clf
    
    grid_search_clf_8.fit(globals.G_X_train, globals.G_y_train)
    grid_search_clf_9=grid_search_clf_8
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(clf5, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(globals.G_current_clf_name_4, out_file)
    with open(snakemake.output[2], 'wb') as out_file:
        pickle.dump(grid_search_clf_9, out_file)
