import sys
import pickle
import globals
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        globals.G_clf_and_params = pickle.load(in_file)
    
    clf= KNeighborsClassifier()
    
    params={'n_neighbors':[5,7,9,11,13,15],
                      'leaf_size':[1,2,3,5],
                      'weights':['uniform', 'distance']
                      }
    
    globals.G_clf_and_params.append((clf, params))
    globals.G_clf_and_params_1=globals.G_clf_and_params
    
    clf=LogisticRegression()
    clf_1=clf
    
    params={'penalty':['l1', 'l2'],
                            'C':np.logspace(0, 4, 10)
                            }
    params_1=params
    
    globals.G_clf_and_params_1.append((clf_1, params_1))
    globals.G_clf_and_params_2=globals.G_clf_and_params_1
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.G_clf_and_params_2, out_file)
