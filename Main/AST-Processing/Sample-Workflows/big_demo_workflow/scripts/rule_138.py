import sys
import pickle
import globals
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from funcs.functions import G_save_result

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        models_2 = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        globals.G_strategy_type = pickle.load(in_file)
    with open(snakemake.input[2], 'rb') as in_file:
        globals.G_submission_id = pickle.load(in_file)
    with open(snakemake.input[3], 'rb') as in_file:
        globals.G_results_2 = pickle.load(in_file)
    with open(snakemake.input[4], 'rb') as in_file:
        globals.G_X_train = pickle.load(in_file)
    with open(snakemake.input[5], 'rb') as in_file:
        globals.G_y_train = pickle.load(in_file)
    with open(snakemake.input[6], 'rb') as in_file:
        globals.G_X_test = pickle.load(in_file)
    with open(snakemake.input[7], 'rb') as in_file:
        clf_and_params = pickle.load(in_file)
    
    clf3 = clf_and_params[2][0]
    
    params3 = clf_and_params[2][1]
    
    globals.G_current_clf_name = clf3.__class__.__name__
    globals.G_current_clf_name_2=globals.G_current_clf_name
    
    grid_search_clf = GridSearchCV(clf3, params3, cv=5)
    grid_search_clf_4=grid_search_clf
    
    grid_search_clf_4.fit(globals.G_X_train, globals.G_y_train)
    grid_search_clf_5=grid_search_clf_4
    
    globals.G_Y_pred = grid_search_clf_5.predict(globals.G_X_test)
    globals.G_Y_pred_2=globals.G_Y_pred
    
    clf_train_acc = round(grid_search_clf_5.score(globals.G_X_train, globals.G_y_train) * 100, 2)
    print(globals.G_current_clf_name_2, " trained and used for prediction on test data...")
    clf_train_acc_2=clf_train_acc
    
    globals.G_results_2[globals.G_current_clf_name_2]=clf_train_acc_2
    globals.G_results_3=globals.G_results_2
    
    models_2.append(clf3)
    G_save_result()
    models_3=models_2
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(models_3, out_file)
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(globals.G_results_3, out_file)
