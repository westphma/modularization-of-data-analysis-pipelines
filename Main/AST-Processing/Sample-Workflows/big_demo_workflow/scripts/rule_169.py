import sys
import pickle
import globals
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from funcs.functions import G_save_result

if __name__ == '__main__':

    with open(snakemake.input[0], 'rb') as in_file:
        clf5 = pickle.load(in_file)
    with open(snakemake.input[1], 'rb') as in_file:
        models_4 = pickle.load(in_file)
    with open(snakemake.input[2], 'rb') as in_file:
        globals.G_strategy_type = pickle.load(in_file)
    with open(snakemake.input[3], 'rb') as in_file:
        globals.G_current_clf_name_4 = pickle.load(in_file)
    with open(snakemake.input[4], 'rb') as in_file:
        globals.G_submission_id = pickle.load(in_file)
    with open(snakemake.input[5], 'rb') as in_file:
        globals.G_results_4 = pickle.load(in_file)
    with open(snakemake.input[6], 'rb') as in_file:
        globals.G_X_train = pickle.load(in_file)
    with open(snakemake.input[7], 'rb') as in_file:
        grid_search_clf_9 = pickle.load(in_file)
    with open(snakemake.input[8], 'rb') as in_file:
        globals.G_y_train = pickle.load(in_file)
    with open(snakemake.input[9], 'rb') as in_file:
        globals.G_X_test = pickle.load(in_file)
    
    globals.G_Y_pred = grid_search_clf_9.predict(globals.G_X_test)
    globals.G_Y_pred_4=globals.G_Y_pred
    
    clf_train_acc = round(grid_search_clf_9.score(globals.G_X_train, globals.G_y_train) * 100, 2)
    print(globals.G_current_clf_name_4, " trained and used for prediction on test data...")
    clf_train_acc_4=clf_train_acc
    
    globals.G_results_4[globals.G_current_clf_name_4]=clf_train_acc_4
    globals.G_results_5=globals.G_results_4
    
    models_4.append(clf5)
    G_save_result()
    models_5=models_4
    
    results5 = "Saved results"
    print()
    with open(snakemake.output[0], 'wb') as out_file:
        pickle.dump(globals.G_results_5, out_file)
