import matplotlib.pyplot as plt
import seaborn as sns


def save_correlation_map(data):
    f,ax = plt.subplots(figsize=(18, 18))
    sns.heatmap(data.corr(), annot=True, linewidths=.5, fmt= '.1f',ax=ax)
    plt.savefig("data/ghost.png")