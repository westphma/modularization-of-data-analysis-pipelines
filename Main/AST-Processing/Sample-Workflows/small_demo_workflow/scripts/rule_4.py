import sys
import pickle
import pandas as pd

if __name__ == '__main__':

    
    data = pd.read_csv(snakemake.input[0])
    
    ghostDF = data[data['against_ghost']>=1.0]
    
    for index, row in data.iterrows():
        row['against_electric'] += 0.1
    data_1=data
    
    data_1.to_csv(snakemake.output[0])
    with open(snakemake.output[1], 'wb') as out_file:
        pickle.dump(ghostDF, out_file)
