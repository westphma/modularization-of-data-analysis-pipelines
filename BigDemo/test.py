# !!!
# This script was created using the Main/Experiments/Decapsulation.ipynb notebook.
# The original (unrefactored) code belongs to Serkan Peldek,
# and can be found on Kaggle: https://www.kaggle.com/code/serkanpeldek/object-oriented-titanics
# !!!

import globals
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import warnings
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import VotingClassifier
from yellowbrick.features import RadViz


def P_drop(data,drop_strategies):
    for column, strategy in drop_strategies.items():
                data=data.drop(labels=[column], axis=strategy)
    return data

def D_fill_arrays():
    a1 = np.array([1, 2, 3, 4, 5])
    a2 = np.zeros(10000)
    np.append(a1, 6)
    np.append(a1, 7)
    np.append(a1, 8)
    a2[0] = 1
    a2[1] = 2
    for i in range(0,len(a2)):
                a2[i] = 1
    return a1, a2

def O__get_train_and_test():
    globals.O_X_train=globals.O_all_data[:globals.O_number_of_train]
    globals.O_X_test=globals.O_all_data[globals.O_number_of_train:]

def O__get_all_data():
    return pd.concat([globals.O_train, globals.O_test])

def G_save_result():
    Submission = pd.DataFrame({'PassengerId': globals.G_submission_id,
                                               'Survived': globals.G_Y_pred})
    file_name="{}_{}.csv".format(globals.G_strategy_type,globals.G_current_clf_name.lower())
    Submission.to_csv(file_name, index=False)
    print("Submission saved file name: ",file_name)

def D_increment1(i):
    globals.D_dummy_array1[i] = 1 + globals.D_dummy_array1[i]

def D_initialize_arrays():
    a1, a2 = D_fill_arrays()
    globals.D_dummy_array1 = a1
    globals.D_dummy_array2 = a2

def D_increment2(i):
    globals.D_dummy_array2[i] = 1 + globals.D_dummy_array2[i]




print(os.listdir("data"))
warnings.filterwarnings('ignore')
print("Warnings were ignored")
train = pd.read_csv("data/train.csv")
test = pd.read_csv("data/test.csv")
print("ObjectOrientedTitanic object created")
globals.O_testPassengerID=test['PassengerId']
globals.O_number_of_train=train.shape[0]
globals.O_y_train=train['Survived']
globals.O_train=train.drop('Survived', axis=1)
globals.O_test=test
globals.O_all_data=O__get_all_data()
print("Information object created")
globals.S_dats=None
print("Preprocess object created")
print("Visualizer object created!")
print("GridSearchHelper Created")
globals.G_gridSearchCV=None
globals.G_clf_and_params=list()
clf= KNeighborsClassifier()
params={'n_neighbors':[5,7,9,11,13,15],
                  'leaf_size':[1,2,3,5],
                  'weights':['uniform', 'distance']
                  }
globals.G_clf_and_params.append((clf, params))
clf=LogisticRegression()
params={'penalty':['l1', 'l2'],
                        'C':np.logspace(0, 4, 10)
                        }
globals.G_clf_and_params.append((clf, params))
clf = SVC()
params = [ {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
                           {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']}]
globals.G_clf_and_params.append((clf, params))
clf=DecisionTreeClassifier()
params={'max_features': ['auto', 'sqrt', 'log2'],
                  'min_samples_split': [2,3,4,5,6,7,8,9,10,11,12,13,14,15],
                  'min_samples_leaf':[1],
                  'random_state':[123]}
globals.G_clf_and_params.append((clf,params))
clf = RandomForestClassifier()
params = {'n_estimators': [4, 6, 9],
                      'max_features': ['log2', 'sqrt','auto'],
                      'criterion': ['entropy', 'gini'],
                      'max_depth': [2, 3, 5, 10],
                      'min_samples_split': [2, 3, 5],
                      'min_samples_leaf': [1,5,8]
                     }
globals.G_clf_and_params.append((clf, params))
feature_dtypes=globals.O_all_data.dtypes
missing_values = globals.O_all_data.isnull().sum()
missing_values = missing_values.sort_values(ascending=False)
globals.I_missing_values = missing_values
print("=" * 50)
print("{:16} {:16} {:25} {:16}".format("Feature Name".upper(),
                                                            "Data Format".upper(),
                                                            "# of Missing Values".upper(),
                                                            "Samples".upper()))
for feature_name, dtype, missing_value in zip(globals.I_missing_values.index.values,
                                              feature_dtypes[globals.I_missing_values.index.values],
                                              globals.I_missing_values.values):
    print("{:18} {:19} {:19} ".format(feature_name, str(dtype), str(missing_value)), end="")
    for v in globals.O_all_data[feature_name].values[:10]:
        print(v, end=",")
    print()

print("="*50)
globals.O_strategy_type='strategy1'
globals.S_dats=O__get_all_data()
drop_strategy = {'PassengerId': 1,  
                                 'Cabin': 1,
                                 'Ticket': 1}
globals.S_dats = P_drop(globals.S_dats, drop_strategy)
fill_strategy = {'Age': 'Median',
                                 'Fare': 'Median',
                                 'Embarked': 'Mode'}
for column, strategy in fill_strategy.items():
    if strategy == 'None':
        globals.S_dats[column] = globals.S_dats[column].fillna('None')
    elif strategy == 'Zero':
        globals.S_dats[column] = globals.S_dats[column].fillna(0)
    elif strategy == 'Mode':
        globals.S_dats[column] = globals.S_dats[column].fillna(globals.S_dats[column].mode()[0])
    elif strategy == 'Mean':
        globals.S_dats[column] = globals.S_dats[column].fillna(globals.S_dats[column].mean())
    elif strategy == 'Median':
        globals.S_dats[column] = globals.S_dats[column].fillna(globals.S_dats[column].median())
    else:
        print("{}: There is no such thing as preprocess strategy".format(strategy))

globals.S_dats = globals.S_dats

globals.S_dats['FamilySize'] = globals.S_dats['SibSp'] + globals.S_dats['Parch'] + 1

globals.S_dats['IsAlone'] = 1
globals.S_dats.loc[(globals.S_dats['FamilySize'] > 1), 'IsAlone'] = 0

globals.S_dats['Title'] = globals.S_dats['Name'].str.split(", ", expand=True)[1].str.split('.', expand=True)[0]
min_lengtht = 10
title_names = (globals.S_dats['Title'].value_counts() < min_lengtht)
globals.S_dats['Title'] = globals.S_dats['Title'].apply(lambda x: 'Misc' if title_names.loc[x] == True else x)

globals.S_dats = globals.S_dats   

globals.S_dats['FareBin'] = pd.qcut(globals.S_dats['Fare'], 4)

globals.S_dats['AgeBin'] = pd.cut(globals.S_dats['Age'].astype(int), 5)

drop_strategy = {'Age': 1,  
                    'Name': 1,
                    'Fare': 1}
globals.S_dats = P_drop(globals.S_dats, drop_strategy)

globals.S_dats = globals.S_dats


labelEncoder=LabelEncoder()
for column in globals.S_dats.columns.values:
    if 'int64'==globals.S_dats[column].dtype or 'float64'==globals.S_dats[column].dtype or 'int64'==globals.S_dats[column].dtype:
        continue
    labelEncoder.fit(globals.S_dats[column])
    globals.S_dats[column]=labelEncoder.transform(globals.S_dats[column])

globals.S_dats = globals.S_dats

non_dummies = list()
for col in globals.S_dats.columns.values:
    if col not in ['Pclass', 'Sex', 'Parch', 'Embarked', 'Title', 'IsAlone']:
        non_dummies.append(col)

columns=['Pclass', 'Sex', 'Parch', 'Embarked', 'Title', 'IsAlone']

dummies_dat = list()
for col in columns:
    dummies_dat.append(pd.get_dummies(globals.S_dats[col],prefix=col))

for non_dummy in non_dummies:
    dummies_dat.append(globals.S_dats[non_dummy])

globals.S_dats = pd.concat(dummies_dat, axis=1)
globals.O_all_data = globals.S_dats
O__get_train_and_test()
#if 'RadViz'=="RadViz":
if None is None:
    features=globals.O_X_train.columns.values
else:
    features=globals.O_X_train.columns.values[:None]

fig, ax=plt.subplots(1, figsize=(15,12))
radViz=RadViz(classes=['survived', 'not survived'], features=features)

radViz.fit(globals.O_X_train, globals.O_y_train)
radViz.transform(globals.O_X_train)
radViz.poof()

O__get_train_and_test()
globals.G_X_train=globals.O_X_train
globals.G_X_test=globals.O_X_test
globals.G_y_train=globals.O_y_train
globals.G_submission_id=globals.O_testPassengerID
globals.G_strategy_type=globals.O_strategy_type
clf_and_params = globals.G_clf_and_params

models=[]
globals.G_results={}
clf1 = clf_and_params[0][0]
params1 = clf_and_params[0][1]

globals.G_current_clf_name = clf1.__class__.__name__
grid_search_clf = GridSearchCV(clf1, params1, cv=5)
grid_search_clf.fit(globals.G_X_train, globals.G_y_train)
globals.G_Y_pred = grid_search_clf.predict(globals.G_X_test)
clf_train_acc = round(grid_search_clf.score(globals.G_X_train, globals.G_y_train) * 100, 2)
print(globals.G_current_clf_name, " trained and used for prediction on test data...")
globals.G_results[globals.G_current_clf_name]=clf_train_acc
models.append(clf1)

G_save_result()
results1 = "Saved results"
print()

clf2 = clf_and_params[1][0]
params2 = clf_and_params[1][1]

globals.G_current_clf_name = clf2.__class__.__name__
grid_search_clf = GridSearchCV(clf2, params2, cv=5)
grid_search_clf.fit(globals.G_X_train, globals.G_y_train)
globals.G_Y_pred = grid_search_clf.predict(globals.G_X_test)
clf_train_acc = round(grid_search_clf.score(globals.G_X_train, globals.G_y_train) * 100, 2)
print(globals.G_current_clf_name, " trained and used for prediction on test data...")
globals.G_results[globals.G_current_clf_name]=clf_train_acc
models.append(clf2)

G_save_result()
results2 = "Saved results"
print()

clf3 = clf_and_params[2][0]
params3 = clf_and_params[2][1]

globals.G_current_clf_name = clf3.__class__.__name__
grid_search_clf = GridSearchCV(clf3, params3, cv=5)
grid_search_clf.fit(globals.G_X_train, globals.G_y_train)
globals.G_Y_pred = grid_search_clf.predict(globals.G_X_test)
clf_train_acc = round(grid_search_clf.score(globals.G_X_train, globals.G_y_train) * 100, 2)
print(globals.G_current_clf_name, " trained and used for prediction on test data...")
globals.G_results[globals.G_current_clf_name]=clf_train_acc
models.append(clf3)

G_save_result()
results3 = "Saved results"
print()

clf4 = clf_and_params[3][0]
params4 = clf_and_params[3][1]

globals.G_current_clf_name = clf4.__class__.__name__
grid_search_clf = GridSearchCV(clf4, params4, cv=5)
grid_search_clf.fit(globals.G_X_train, globals.G_y_train)
globals.G_Y_pred = grid_search_clf.predict(globals.G_X_test)
clf_train_acc = round(grid_search_clf.score(globals.G_X_train, globals.G_y_train) * 100, 2)
print(globals.G_current_clf_name, " trained and used for prediction on test data...")
globals.G_results[globals.G_current_clf_name]=clf_train_acc
models.append(clf4)

G_save_result()
results4 = "Saved results"
print()

clf5 = clf_and_params[4][0]
params5 = clf_and_params[4][1]

globals.G_current_clf_name = clf5.__class__.__name__
grid_search_clf = GridSearchCV(clf5, params5, cv=5)
grid_search_clf.fit(globals.G_X_train, globals.G_y_train)
globals.G_Y_pred = grid_search_clf.predict(globals.G_X_test)
clf_train_acc = round(grid_search_clf.score(globals.G_X_train, globals.G_y_train) * 100, 2)
print(globals.G_current_clf_name, " trained and used for prediction on test data...")
globals.G_results[globals.G_current_clf_name]=clf_train_acc
models.append(clf5)

G_save_result()
results5 = "Saved results"
print()

for clf_name, train_acc in globals.G_results.items():
          print("{} train accuracy is {:.3f}".format(clf_name, train_acc))

D_initialize_arrays()
D_increment1(0)
D_increment1(0)
D_initialize_arrays()
D_increment2(1)
D_increment2(2)
for i in range(0,100):
    D_increment2(i)